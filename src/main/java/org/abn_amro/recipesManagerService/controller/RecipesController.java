package org.abn_amro.recipesManagerService.controller;

import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.dto.request.*;
import org.abn_amro.recipesManagerService.dto.response.IngredientDTO;
import org.abn_amro.recipesManagerService.dto.response.ListResponseDTO;
import org.abn_amro.recipesManagerService.dto.response.RecipesDTO;
import org.abn_amro.recipesManagerService.service.IRecipesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/recipes")
@Validated
public class RecipesController {

    private final IRecipesService service;

    public RecipesController(IRecipesService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<RecipesDTO> getRecipesById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getRecipesById(id), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<ListResponseDTO<RecipesDTO>> getAllRecipes(@Valid GetRecipesListDTO getRecipesListDTO) {
        return new ResponseEntity<>(service.getRecipesList(getRecipesListDTO), HttpStatus.OK);
    }

    @PostMapping("/filter")
    public ResponseEntity<ListResponseDTO<RecipesDTO>> getFilteredRecipes(@RequestBody FilterRecipesDTO filterRecipesDTO) {
        return new ResponseEntity<>(service.getFilteredRecipes(filterRecipesDTO), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<RecipesDTO> addRecipes(@Validated(GroupCheck.OrderedCheck.class)
                                                 @RequestBody AddRecipesDTO addRecipesDTO) {
        return new ResponseEntity<>(service.addRecipes(addRecipesDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RecipesDTO> deleteRecipes(@PathVariable Long id) {
        return new ResponseEntity<>(service.deleteRecipes(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<RecipesDTO> editRecipes(@Validated(GroupCheck.OrderedCheck.class)
                                                  @RequestBody EditRecipesDTO editRecipesDTO) {
        return new ResponseEntity<>(service.editRecipes(editRecipesDTO), HttpStatus.OK);
    }

    @PutMapping("/ingredient")
    public ResponseEntity<IngredientDTO> editRecipesIngredient(@Validated(GroupCheck.OrderedCheck.class)
                                                               @RequestBody EditIngredientDTO editIngredientDTO) {
        return new ResponseEntity<>(service.editRecipesIngredient(editIngredientDTO), HttpStatus.OK);
    }

    @PostMapping("/ingredient")
    public ResponseEntity<IngredientDTO> addRecipesIngredient(@Validated(GroupCheck.OrderedCheck.class)
                                                              @RequestBody AddNewIngredientDTO addIngredientDTO) {
        return new ResponseEntity<>(service.addRecipesIngredient(addIngredientDTO), HttpStatus.OK);
    }

    @DeleteMapping("/ingredient")
    public ResponseEntity<IngredientDTO> deleteRecipesIngredient(@Validated(GroupCheck.OrderedCheck.class)
                                                                 @RequestBody DeleteIngredientDTO deleteIngredientDTO) {
        service.deleteRecipesIngredient(deleteIngredientDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
