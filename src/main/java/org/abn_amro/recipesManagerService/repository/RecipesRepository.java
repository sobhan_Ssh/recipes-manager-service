package org.abn_amro.recipesManagerService.repository;

import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipesRepository extends JpaRepository<Recipes, Long> {



    Page<Recipes> findByTypeAndStatus(RecipesType type, RecipesStatus status, Pageable page);

    Page<Recipes> findByStatus(RecipesStatus status, Pageable page);

    @Query(value = "select r from Recipes r " +
            "where ((:typ) is null or r.type in :typ) " +
            "and ((:cnt)  is null or r.serveCount in :cnt) " +
            "and ((:inc) is null or exists (select 1 from Ingredient i where i.recipes = r and i.name in :inc)) " +
            "and ((:exc) is null or not exists (select 1 from Ingredient i where i.recipes = r and i.name in :exc)) " +
            "and r.status = :status")
    Optional<Page<Recipes>> findFilteredRecipes(@Param("typ") List<RecipesType> types,
                                                @Param("cnt") List<Integer> serveCounts,
                                                @Param("inc") List<String> includeIngredients,
                                                @Param("exc") List<String> excludeIngredients,
                                                @Param("status") RecipesStatus status,
                                                Pageable page);

}
