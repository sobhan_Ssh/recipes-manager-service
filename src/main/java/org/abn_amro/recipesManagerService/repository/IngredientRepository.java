package org.abn_amro.recipesManagerService.repository;

import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    Optional<List<Ingredient>> findByRecipes_Id(Long recipesId);

    @Modifying
    @Query("delete from Ingredient i where i.id = :id")
    void deleteById(Long id);
}
