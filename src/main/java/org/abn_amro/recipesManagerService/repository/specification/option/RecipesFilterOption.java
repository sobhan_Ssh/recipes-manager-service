package org.abn_amro.recipesManagerService.repository.specification.option;

public enum RecipesFilterOption {

    SERVE_COUNT,
    RECIPES_TYPE,
    INCLUDE_INGREDIENTS,
    EXCLUDE_INGREDIENTS
}
