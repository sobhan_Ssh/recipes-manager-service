package org.abn_amro.recipesManagerService.repository.specification.criteria;

import org.abn_amro.recipesManagerService.repository.specification.option.RecipesFilterOption;

import java.util.List;

public class RecipesFilterCriteria {

    private String key;
    private Object value;
    private RecipesFilterOption option;
    private List<Long> IDs;

    public RecipesFilterCriteria() {
    }

    public RecipesFilterCriteria(Object value, RecipesFilterOption option, List<Long> IDs) {
        this.value = value;
        this.option = option;
        this.IDs = IDs;
    }

    public RecipesFilterCriteria(String key, RecipesFilterOption option, List<Long> IDs) {
        this.key = key;
        this.option = option;
        this.IDs = IDs;
    }

    public String getKey() {
        return key;
    }

    public RecipesFilterCriteria setKey(String key) {
        this.key = key;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public RecipesFilterCriteria setValue(Object value) {
        this.value = value;
        return this;
    }

    public RecipesFilterOption getOption() {
        return option;
    }

    public RecipesFilterCriteria setOption(RecipesFilterOption option) {
        this.option = option;
        return this;
    }

    public List<Long> getIDs() {
        return IDs;
    }

    public RecipesFilterCriteria setIDs(List<Long> IDs) {
        this.IDs = IDs;
        return this;
    }
}
