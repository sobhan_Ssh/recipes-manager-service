package org.abn_amro.recipesManagerService.repository.specification;

import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.entity.Recipes_;
import org.abn_amro.recipesManagerService.repository.specification.criteria.RecipesFilterCriteria;
import org.abn_amro.recipesManagerService.repository.specification.option.RecipesFilterOption;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class RecipesSpecification implements Specification<Recipes> {

    private List<RecipesFilterCriteria> list;
    public RecipesSpecification() {
        this.list = new ArrayList<>();
    }
    public void add(RecipesFilterCriteria criteria) {
        list.add(criteria);
    }


    @Override
    public Predicate toPredicate(Root<Recipes> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        query.orderBy(builder.desc(root.get(Recipes_.ID)));
        for (RecipesFilterCriteria criteria : list) {
            if (criteria.getOption().equals(RecipesFilterOption.RECIPES_TYPE)) {

            } else if (criteria.getOption().equals(RecipesFilterOption.SERVE_COUNT)) {

            } else if (criteria.getOption().equals(RecipesFilterOption.INCLUDE_INGREDIENTS)) {

            } else if (criteria.getOption().equals(RecipesFilterOption.EXCLUDE_INGREDIENTS)) {

            }
        }

        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
