package org.abn_amro.recipesManagerService.util;

import java.sql.Timestamp;

public class Utility {

    public static Timestamp getCurrentDate() {
        return new Timestamp(System.currentTimeMillis());
    }

}
