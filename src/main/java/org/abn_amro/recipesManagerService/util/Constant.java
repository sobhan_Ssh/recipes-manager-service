package org.abn_amro.recipesManagerService.util;

/**
 * @author S.shakery
 */

public class Constant {
    public static final String DEFAULT_UNEXPECTED_ERROR = "unexpected error";
    public static final String RECIPES_ALREADY_DELETED = "recipes already deleted";
    public static final String RECIPES_NOT_FOUND = "recipes not found";
    public static final String INGREDIENT_NOT_FOUND = "ingredient not found";
    public static final String INVALID_RECIPES_TYPE = "invalid recipes type";
    public static final String INVALID_RECIPES_STATUS = "invalid recipes status";
    public static final String INVALID_REQUEST = "request is invalid";
    public static final String REQUIRED_NAME = "name is required";
    public static final String REQUIRED_INSTRUCTION = "instruction is required";
    public static final String REQUIRED_UNIT = "unit is required";
    public static final String REQUIRED_COUNT = "count is required";
    public static final String REQUIRED_SERVE_COUNT = "serve count is required";
    public static final String REQUIRED_RECIPES_ID = "recipes id is required";
    public static final String REQUIRED_INGREDIENT_ID = "ingredient id is required";

}
