package org.abn_amro.recipesManagerService.service;

import org.abn_amro.recipesManagerService.dto.request.*;
import org.abn_amro.recipesManagerService.dto.response.IngredientDTO;
import org.abn_amro.recipesManagerService.dto.response.ListResponseDTO;
import org.abn_amro.recipesManagerService.dto.response.RecipesDTO;

public interface IRecipesService {

    RecipesDTO getRecipesById(Long id);

    ListResponseDTO<RecipesDTO> getRecipesList(GetRecipesListDTO getRecipesListDTO);

    RecipesDTO addRecipes(AddRecipesDTO addRecipesDTO);

    RecipesDTO deleteRecipes(Long id);

    RecipesDTO editRecipes(EditRecipesDTO editRecipesDTO);

    IngredientDTO addRecipesIngredient(AddNewIngredientDTO addIngredientDTO);
    void deleteRecipesIngredient(DeleteIngredientDTO deleteIngredientDTO);

    IngredientDTO editRecipesIngredient(EditIngredientDTO editIngredientDTO);

    ListResponseDTO<RecipesDTO> getFilteredRecipes(FilterRecipesDTO filterRecipesDTO);
}
