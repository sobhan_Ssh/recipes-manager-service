package org.abn_amro.recipesManagerService.service.impl;

import org.abn_amro.recipesManagerService.dto.request.*;
import org.abn_amro.recipesManagerService.dto.response.IngredientDTO;
import org.abn_amro.recipesManagerService.dto.response.ListResponseDTO;
import org.abn_amro.recipesManagerService.dto.response.RecipesDTO;
import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;
import org.abn_amro.recipesManagerService.exception.AlreadyDeletedException;
import org.abn_amro.recipesManagerService.exception.NotFoundException;
import org.abn_amro.recipesManagerService.repository.IngredientRepository;
import org.abn_amro.recipesManagerService.repository.RecipesRepository;
import org.abn_amro.recipesManagerService.service.IRecipesService;
import org.abn_amro.recipesManagerService.util.Constant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RecipesServiceImpl implements IRecipesService {

    private final RecipesRepository repository;
    private final IngredientRepository ingredientRepository;

    public RecipesServiceImpl(RecipesRepository repository, IngredientRepository ingredientRepository) {
        this.repository = repository;
        this.ingredientRepository = ingredientRepository;
    }

    private Recipes findRecipesById(Long id) {
        var temp = repository.findById(id);
        if (temp.isPresent()) {
            if (Objects.equals(temp.get().getStatus(), RecipesStatus.DELETED))
                throw new AlreadyDeletedException(Constant.RECIPES_ALREADY_DELETED);
            else return temp.get();
        } else
            throw new NotFoundException(Constant.RECIPES_NOT_FOUND);
    }

    @Override
    public RecipesDTO getRecipesById(Long id) {
        return new RecipesDTO().from(findRecipesById(id));
    }

    @Override
    public ListResponseDTO<RecipesDTO> getRecipesList(GetRecipesListDTO getRecipesListDTO) {
        ListResponseDTO<RecipesDTO> response = new ListResponseDTO<>();
        Page<Recipes> temp;
        if (Objects.isNull(getRecipesListDTO.getType()))
            temp = repository.findByStatus(RecipesStatus.ACTIVE, PageRequest.of(getRecipesListDTO.getPage(), getRecipesListDTO.getSize()));
        else
            temp = repository.findByTypeAndStatus(RecipesType.parseRecipesType(getRecipesListDTO.getType()),
                    RecipesStatus.ACTIVE,
                    PageRequest.of(getRecipesListDTO.getPage(), getRecipesListDTO.getSize()));

        if (!temp.isEmpty()) {
            response = new ListResponseDTO<>(getRecipesListDTO.getPage(), temp.getNumberOfElements());
            for (Recipes recipes : temp)
                response.getData().add(new RecipesDTO().from(recipes));
        }
        return response;
    }

    @Override
    public RecipesDTO addRecipes(AddRecipesDTO addRecipesDTO) {
        Recipes recipes = repository.save(new Recipes().from(addRecipesDTO));
        List<Ingredient> ingredientList = new ArrayList<>();
        for (AddIngredientDTO ingredientDTO : addRecipesDTO.getIngredients()) {
            ingredientList.add(new Ingredient().from(ingredientDTO).setRecipes(recipes));
        }
        recipes.setIngredients(ingredientRepository.saveAll(ingredientList));
        return new RecipesDTO().from(recipes);
    }

    @Override
    public RecipesDTO deleteRecipes(Long id) {
        var temp = findRecipesById(id);
        return new RecipesDTO().from(repository.save(temp.setStatus(RecipesStatus.DELETED)));
    }

    @Override
    public RecipesDTO editRecipes(EditRecipesDTO editRecipesDTO) {
        var temp = findRecipesById(editRecipesDTO.getId());

        if (Objects.nonNull(editRecipesDTO.getName()) && !editRecipesDTO.getName().isEmpty())
            temp.setName(editRecipesDTO.getName());
        if (Objects.nonNull(editRecipesDTO.getType()))
            temp.setType(RecipesType.parseRecipesType(Math.toIntExact(editRecipesDTO.getType())));
        if (Objects.nonNull(editRecipesDTO.getInstruction()) && !editRecipesDTO.getInstruction().isEmpty())
            temp.setInstruction(editRecipesDTO.getInstruction());
        if (Objects.nonNull(editRecipesDTO.getServeCount()))
            temp.setServeCount(editRecipesDTO.getServeCount());
        return new RecipesDTO().from(repository.save(temp));
    }

    @Override
    public IngredientDTO addRecipesIngredient(AddNewIngredientDTO addIngredientDTO) {
        var tempRecipes = findRecipesById(addIngredientDTO.getRecipesId());

        return new IngredientDTO().from(ingredientRepository.save(
                new Ingredient()
                        .setRecipes(tempRecipes)
                        .setCount(addIngredientDTO.getCount())
                        .setInstruction(addIngredientDTO.getInstruction())
                        .setUnit(addIngredientDTO.getUnit())
                        .setName(addIngredientDTO.getName()))
        );
    }

    @Override
    public void deleteRecipesIngredient(DeleteIngredientDTO deleteIngredientDTO) {
        var tempRecipes = findRecipesById(deleteIngredientDTO.getRecipesId());
        var deleteIngredient = tempRecipes.getIngredients().stream().filter(
                        ingredient -> Objects.equals(ingredient.getId(), deleteIngredientDTO.getIngredientId()))
                .findFirst().orElseThrow(() -> new NotFoundException(Constant.INGREDIENT_NOT_FOUND));
        ingredientRepository.deleteById(deleteIngredient.getId());
    }

    @Override
    public IngredientDTO editRecipesIngredient(EditIngredientDTO editIngredientDTO) {
        var tempRecipes = findRecipesById(editIngredientDTO.getRecipesId());

        var editIngredient = tempRecipes.getIngredients().stream().filter(
                        ingredient -> Objects.equals(ingredient.getId(), editIngredientDTO.getRecipesId()))
                .findFirst().orElseThrow(() -> new NotFoundException(Constant.INGREDIENT_NOT_FOUND));

        if (Objects.nonNull(editIngredientDTO.getName()) && !editIngredientDTO.getName().isEmpty())
            editIngredient.setName(editIngredientDTO.getName());
        if (Objects.nonNull(editIngredientDTO.getCount()))
            editIngredient.setCount(editIngredientDTO.getCount());
        if (Objects.nonNull(editIngredientDTO.getUnit()) && !editIngredientDTO.getUnit().isEmpty())
            editIngredient.setUnit(editIngredientDTO.getUnit());
        if (Objects.nonNull(editIngredientDTO.getInstruction()) && !editIngredientDTO.getInstruction().isEmpty())
            editIngredient.setInstruction(editIngredientDTO.getInstruction());
        return new IngredientDTO().from(ingredientRepository.save(editIngredient));
    }

    @Override
    public ListResponseDTO<RecipesDTO> getFilteredRecipes(FilterRecipesDTO filterRecipesDTO) {
        ListResponseDTO<RecipesDTO> response = new ListResponseDTO<>();
        var temp = repository.findFilteredRecipes(
                filterRecipesDTO.getTypes().size() > 0 ? RecipesType.parseRecipesTypeList(filterRecipesDTO.getTypes()) : null,
                filterRecipesDTO.getServeCounts().size() > 0 ? filterRecipesDTO.getServeCounts() : null,
                filterRecipesDTO.getIncludeIngredients().size() > 0 ? filterRecipesDTO.getIncludeIngredients() : null,
                filterRecipesDTO.getExcludeIngredients().size() > 0 ? filterRecipesDTO.getExcludeIngredients() : null,
                RecipesStatus.ACTIVE,
                PageRequest.of(filterRecipesDTO.getPage(), filterRecipesDTO.getSize()));

        if (temp.isPresent()) {
            response = new ListResponseDTO<>(filterRecipesDTO.getPage(), temp.get().getNumberOfElements());
            for (Recipes recipes : temp.get().getContent())
                response.getData().add(new RecipesDTO().from(recipes));
        }
        return response;
    }


}
