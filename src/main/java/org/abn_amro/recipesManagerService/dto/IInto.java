package org.abn_amro.recipesManagerService.dto;

public interface IInto<DestinationType> {

    DestinationType into(DestinationType destination);

}
