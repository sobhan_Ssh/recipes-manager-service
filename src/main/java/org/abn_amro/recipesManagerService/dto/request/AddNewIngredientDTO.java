package org.abn_amro.recipesManagerService.dto.request;

import org.abn_amro.recipesManagerService.annotation.AddNewIngredientRequest;
import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AddNewIngredientRequest(groups = GroupCheck.ClassCheck.class)
public class AddNewIngredientDTO {

    @NotNull
    private Long recipesId;

    @NotEmpty(message = Constant.REQUIRED_NAME)
    @NotNull(message = Constant.REQUIRED_NAME)
    private String name;

    @NotNull(message = Constant.REQUIRED_COUNT)
    private Integer count;

    @NotEmpty(message = Constant.REQUIRED_UNIT)
    @NotNull(message = Constant.REQUIRED_UNIT)
    private String unit;

    @NotEmpty(message = Constant.REQUIRED_INSTRUCTION)
    @NotNull(message = Constant.REQUIRED_INSTRUCTION)
    private String instruction;

    public Long getRecipesId() {
        return recipesId;
    }

    public AddNewIngredientDTO setRecipesId(Long recipesId) {
        this.recipesId = recipesId;
        return this;
    }

    public String getName() {
        return name;
    }

    public AddNewIngredientDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getCount() {
        return count;
    }

    public AddNewIngredientDTO setCount(Integer count) {
        this.count = count;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public AddNewIngredientDTO setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public AddNewIngredientDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }
}
