package org.abn_amro.recipesManagerService.dto.request;

public class GetRecipesListDTO extends PaginationDTO {

    private Integer type = null;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
