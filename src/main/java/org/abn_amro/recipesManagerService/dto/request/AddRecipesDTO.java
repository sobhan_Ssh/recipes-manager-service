package org.abn_amro.recipesManagerService.dto.request;

import org.abn_amro.recipesManagerService.annotation.AddRecipesRequest;
import org.abn_amro.recipesManagerService.annotation.RecipesType;
import org.abn_amro.recipesManagerService.annotation.group.AdvancedCheck;
import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@AddRecipesRequest(groups = GroupCheck.ClassCheck.class)
public class AddRecipesDTO {

    @NotEmpty(message = Constant.REQUIRED_NAME)
    @NotNull(message = Constant.REQUIRED_NAME)
    private String name;

    @NotNull(message = Constant.REQUIRED_SERVE_COUNT)
    private Integer serveCount;

    @RecipesType(groups = AdvancedCheck.class)
    private Integer type;

    @NotEmpty(message = Constant.REQUIRED_INSTRUCTION)
    @NotNull(message = Constant.REQUIRED_INSTRUCTION)
    private String instruction;

    private List<AddIngredientDTO> ingredients = new ArrayList<>();

    public String getName() {
        return name;
    }

    public AddRecipesDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getServeCount() {
        return serveCount;
    }

    public AddRecipesDTO setServeCount(Integer serveCount) {
        this.serveCount = serveCount;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public AddRecipesDTO setType(Integer type) {
        this.type = type;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public AddRecipesDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    public List<AddIngredientDTO> getIngredients() {
        return ingredients;
    }

    public AddRecipesDTO setIngredients(List<AddIngredientDTO> ingredients) {
        this.ingredients = ingredients;
        return this;
    }
}
