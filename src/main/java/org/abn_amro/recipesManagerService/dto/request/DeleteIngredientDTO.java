package org.abn_amro.recipesManagerService.dto.request;

import org.abn_amro.recipesManagerService.annotation.DeleteIngredientRequest;
import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.constraints.NotNull;

@DeleteIngredientRequest(groups = GroupCheck.ClassCheck.class)
public class DeleteIngredientDTO {

    @NotNull(message = Constant.REQUIRED_RECIPES_ID)
    private Long recipesId;

    @NotNull(message = Constant.REQUIRED_INGREDIENT_ID)
    private Long ingredientId;

    public Long getRecipesId() {
        return recipesId;
    }

    public DeleteIngredientDTO setRecipesId(Long recipesId) {
        this.recipesId = recipesId;
        return this;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public DeleteIngredientDTO setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
        return this;
    }
}
