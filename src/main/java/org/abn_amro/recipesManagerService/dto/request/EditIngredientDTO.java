package org.abn_amro.recipesManagerService.dto.request;

import org.abn_amro.recipesManagerService.annotation.EditIngredientRequest;
import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.constraints.NotNull;

@EditIngredientRequest(groups = GroupCheck.ClassCheck.class)
public class EditIngredientDTO {

    @NotNull(message = Constant.REQUIRED_RECIPES_ID)
    private Long recipesId;

    @NotNull(message = Constant.REQUIRED_INGREDIENT_ID)
    private Long ingredientId;

    private String name;

    private Integer count;

    private String unit;

    private String instruction;

    public Long getRecipesId() {
        return recipesId;
    }

    public EditIngredientDTO setRecipesId(Long recipesId) {
        this.recipesId = recipesId;
        return this;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public EditIngredientDTO setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
        return this;
    }

    public String getName() {
        return name;
    }

    public EditIngredientDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getCount() {
        return count;
    }

    public EditIngredientDTO setCount(Integer count) {
        this.count = count;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public EditIngredientDTO setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public EditIngredientDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }
}
