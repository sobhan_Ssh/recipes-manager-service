package org.abn_amro.recipesManagerService.dto.request;

import java.util.ArrayList;
import java.util.List;

public class FilterRecipesDTO extends PaginationDTO {

    private List<String> includeIngredients = new ArrayList<>();
    private List<String> excludeIngredients = new ArrayList<>();
    private List<Integer> types = new ArrayList<>();
    private List<Integer> serveCounts = new ArrayList<>();

    public List<String> getIncludeIngredients() {
        return includeIngredients;
    }

    public void setIncludeIngredients(List<String> includeIngredients) {
        this.includeIngredients = includeIngredients;
    }

    public List<String> getExcludeIngredients() {
        return excludeIngredients;
    }

    public void setExcludeIngredients(List<String> excludeIngredients) {
        this.excludeIngredients = excludeIngredients;
    }

    public List<Integer> getTypes() {
        return types;
    }

    public FilterRecipesDTO setTypes(List<Integer> types) {
        this.types = types;
        return this;
    }

    public List<Integer> getServeCounts() {
        return serveCounts;
    }

    public FilterRecipesDTO setServeCounts(List<Integer> serveCounts) {
        this.serveCounts = serveCounts;
        return this;
    }
}
