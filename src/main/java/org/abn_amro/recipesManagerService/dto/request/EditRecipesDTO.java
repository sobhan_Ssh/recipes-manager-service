package org.abn_amro.recipesManagerService.dto.request;


import org.abn_amro.recipesManagerService.annotation.EditRecipesRequest;
import org.abn_amro.recipesManagerService.annotation.RecipesType;
import org.abn_amro.recipesManagerService.annotation.group.AdvancedCheck;
import org.abn_amro.recipesManagerService.annotation.group.GroupCheck;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EditRecipesRequest(groups = GroupCheck.ClassCheck.class)
public class EditRecipesDTO {

    @NotNull(message = Constant.REQUIRED_RECIPES_ID)
    private Long id;

    private String name;

    private Integer serveCount;

    private Integer type;

    private String instruction;

    public Long getId() {
        return id;
    }

    public EditRecipesDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public EditRecipesDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getServeCount() {
        return serveCount;
    }

    public EditRecipesDTO setServeCount(Integer serveCount) {
        this.serveCount = serveCount;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public EditRecipesDTO setType(Integer type) {
        this.type = type;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public EditRecipesDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }
}
