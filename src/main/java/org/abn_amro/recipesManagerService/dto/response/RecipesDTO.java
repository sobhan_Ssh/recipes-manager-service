package org.abn_amro.recipesManagerService.dto.response;

import org.abn_amro.recipesManagerService.dto.IFrom;
import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;

import java.util.HashSet;
import java.util.Set;

public class RecipesDTO implements IFrom<Recipes> {

    private Long id;

    private String name;

    private Integer serveCount;

    private RecipesType type;

    private RecipesStatus status;

    private String instruction;

    private Set<IngredientDTO> ingredients = new HashSet<>();

    public Long getId() {
        return id;
    }

    public RecipesDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RecipesDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getServeCount() {
        return serveCount;
    }

    public RecipesDTO setServeCount(Integer serveCount) {
        this.serveCount = serveCount;
        return this;
    }

    public RecipesType getType() {
        return type;
    }

    public RecipesDTO setType(RecipesType type) {
        this.type = type;
        return this;
    }

    public RecipesStatus getStatus() {
        return status;
    }

    public RecipesDTO setStatus(RecipesStatus status) {
        this.status = status;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public RecipesDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    public Set<IngredientDTO> getIngredients() {
        return ingredients;
    }


    public RecipesDTO setIngredients(Set<IngredientDTO> ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    @Override
    public RecipesDTO from(Recipes source) {
        this
                .setId(source.getId())
                .setStatus(source.getStatus())
                .setType(source.getType())
                .setName(source.getName())
                .setServeCount(source.getServeCount())
                .setInstruction(source.getInstruction());
        for (Ingredient ingredient : source.getIngredients()) {
            this.getIngredients().add(new IngredientDTO().from(ingredient));
        }
        return this;
    }
}
