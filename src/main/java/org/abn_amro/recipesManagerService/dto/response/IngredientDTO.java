package org.abn_amro.recipesManagerService.dto.response;

import org.abn_amro.recipesManagerService.dto.IFrom;
import org.abn_amro.recipesManagerService.entity.Ingredient;

public class IngredientDTO implements IFrom<Ingredient> {

    private Long id;

    private String name;

    private Integer count;

    private String unit;

    private String instruction;

    public Long getId() {
        return id;
    }

    public IngredientDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public IngredientDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getCount() {
        return count;
    }

    public IngredientDTO setCount(Integer count) {
        this.count = count;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public IngredientDTO setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public IngredientDTO setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    @Override
    public IngredientDTO from(Ingredient source) {
        return this
                .setCount(source.getCount())
                .setInstruction(source.getInstruction())
                .setUnit(source.getUnit())
                .setId(source.getId())
                .setName(source.getName());
    }
}
