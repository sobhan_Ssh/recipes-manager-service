package org.abn_amro.recipesManagerService.dto.response;

import java.util.ArrayList;
import java.util.List;

public class ListResponseDTO<T> {

    private int page;
    private int size;
    private List<T> data = new ArrayList<>();

    public ListResponseDTO(int page, int size) {
        this.page = page;
        this.size = size;
        this.data = new ArrayList<>();
    }

    public ListResponseDTO(int page, int size, List<T> data) {
        this.page = page;
        this.size = size;
        this.data = data;
    }

    public ListResponseDTO() {
    }

    public int getPage() {
        return page;
    }

    public ListResponseDTO<T> setPage(int page) {
        this.page = page;
        return this;
    }

    public int getSize() {
        return size;
    }

    public ListResponseDTO<T> setSize(int size) {
        this.size = size;
        return this;
    }

    public List<T> getData() {
        return data;
    }

    public ListResponseDTO<T> setData(List<T> data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "ListResponseDTO{" +
                "page=" + page +
                ", size=" + size +
                ", data=" + data +
                '}';
    }
}
