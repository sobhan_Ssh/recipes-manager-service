package org.abn_amro.recipesManagerService.dto;

public interface IFrom<SourceType> {

    IFrom<SourceType> from(SourceType source);

}
