package org.abn_amro.recipesManagerService.config;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class DefaultResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date date;

    private String message;

    public Date getDate() {
        return date;
    }

    public DefaultResponse setDate(Date date) {
        this.date = date;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public DefaultResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
