package org.abn_amro.recipesManagerService.config;

import org.abn_amro.recipesManagerService.exception.*;
import org.abn_amro.recipesManagerService.util.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(Exception ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setDate(new Date());
        response.setMessage(Constant.DEFAULT_UNEXPECTED_ERROR);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(BadRequestException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(NotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidRecipesTypeException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidRecipesTypeException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler(InvalidRecipesStatusException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidRecipesStatusException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler(AlreadyDeletedException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(AlreadyDeletedException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }
}

