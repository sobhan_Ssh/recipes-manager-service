package org.abn_amro.recipesManagerService.annotation;

import org.abn_amro.recipesManagerService.annotation.validator.AddNewIngredientRequestValidator;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AddNewIngredientRequestValidator.class)
public @interface AddNewIngredientRequest {

    String message() default Constant.INVALID_REQUEST;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
