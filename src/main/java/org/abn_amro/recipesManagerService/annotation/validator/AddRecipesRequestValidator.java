package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.AddRecipesRequest;
import org.abn_amro.recipesManagerService.dto.request.AddRecipesDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class AddRecipesRequestValidator implements ConstraintValidator<AddRecipesRequest, AddRecipesDTO> {
    @Override
    public void initialize(AddRecipesRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(AddRecipesDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getType()) &&
                Objects.nonNull(value.getServeCount()) &&
                Objects.nonNull(value.getInstruction()) &&
                !value.getInstruction().isEmpty() &&
                Objects.nonNull(value.getName()) &&
                !value.getName().isEmpty() &&
                value.getIngredients().size() > 0;
    }
}
