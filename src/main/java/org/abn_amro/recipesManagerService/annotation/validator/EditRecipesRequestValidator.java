package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.EditRecipesRequest;
import org.abn_amro.recipesManagerService.dto.request.EditRecipesDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EditRecipesRequestValidator implements ConstraintValidator<EditRecipesRequest, EditRecipesDTO> {
    @Override
    public void initialize(EditRecipesRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(EditRecipesDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getId());
    }
}
