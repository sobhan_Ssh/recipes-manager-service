package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.RecipesType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class RecipesTypeValidator implements ConstraintValidator<RecipesType, Integer> {
    @Override
    public void initialize(RecipesType constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && org.abn_amro.recipesManagerService.enums.RecipesType.contains(value);
    }
}

