package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.AddNewIngredientRequest;
import org.abn_amro.recipesManagerService.dto.request.AddNewIngredientDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class AddNewIngredientRequestValidator implements ConstraintValidator<AddNewIngredientRequest, AddNewIngredientDTO> {
    @Override
    public void initialize(AddNewIngredientRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(AddNewIngredientDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getCount()) &&
                Objects.nonNull(value.getInstruction()) &&
                !value.getInstruction().isEmpty() &&
                Objects.nonNull(value.getName()) &&
                !value.getName().isEmpty() &&
                Objects.nonNull(value.getUnit()) &&
                !value.getUnit().isEmpty() &&
                Objects.nonNull(value.getRecipesId());
    }
}
