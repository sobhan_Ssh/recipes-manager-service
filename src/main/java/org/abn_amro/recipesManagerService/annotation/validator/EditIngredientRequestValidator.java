package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.EditIngredientRequest;
import org.abn_amro.recipesManagerService.dto.request.EditIngredientDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EditIngredientRequestValidator implements ConstraintValidator<EditIngredientRequest, EditIngredientDTO> {
    @Override
    public void initialize(EditIngredientRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(EditIngredientDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getIngredientId()) &&
                Objects.nonNull(value.getRecipesId());
    }
}
