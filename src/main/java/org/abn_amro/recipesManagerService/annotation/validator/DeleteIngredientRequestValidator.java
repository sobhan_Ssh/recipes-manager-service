package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.DeleteIngredientRequest;
import org.abn_amro.recipesManagerService.dto.request.DeleteIngredientDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class DeleteIngredientRequestValidator implements ConstraintValidator<DeleteIngredientRequest, DeleteIngredientDTO> {
    @Override
    public void initialize(DeleteIngredientRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(DeleteIngredientDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getRecipesId()) &&
                Objects.nonNull(value.getIngredientId());
    }
}
