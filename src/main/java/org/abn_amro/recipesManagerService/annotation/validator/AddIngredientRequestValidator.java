package org.abn_amro.recipesManagerService.annotation.validator;

import org.abn_amro.recipesManagerService.annotation.AddIngredientRequest;
import org.abn_amro.recipesManagerService.dto.request.AddIngredientDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class AddIngredientRequestValidator implements ConstraintValidator<AddIngredientRequest, AddIngredientDTO> {
    @Override
    public void initialize(AddIngredientRequest constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(AddIngredientDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getCount()) &&
                Objects.nonNull(value.getInstruction()) &&
                !value.getInstruction().isEmpty() &&
                Objects.nonNull(value.getName()) &&
                !value.getName().isEmpty() &&
                Objects.nonNull(value.getUnit()) &&
                !value.getUnit().isEmpty();
    }
}
