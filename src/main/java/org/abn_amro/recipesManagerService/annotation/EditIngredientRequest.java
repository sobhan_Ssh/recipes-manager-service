package org.abn_amro.recipesManagerService.annotation;

import org.abn_amro.recipesManagerService.annotation.validator.EditIngredientRequestValidator;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EditIngredientRequestValidator.class)
public @interface EditIngredientRequest {

    String message() default Constant.INVALID_REQUEST;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
