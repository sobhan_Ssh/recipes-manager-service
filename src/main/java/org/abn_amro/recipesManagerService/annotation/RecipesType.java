package org.abn_amro.recipesManagerService.annotation;

import org.abn_amro.recipesManagerService.annotation.validator.RecipesTypeValidator;
import org.abn_amro.recipesManagerService.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(ElementType.FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = RecipesTypeValidator.class)
public @interface RecipesType {

    String message() default Constant.INVALID_RECIPES_TYPE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
