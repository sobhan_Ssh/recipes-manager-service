package org.abn_amro.recipesManagerService.exception;

public class AlreadyDeletedException extends RuntimeException {

    public AlreadyDeletedException(String s) {
        super(s);
    }
}

