package org.abn_amro.recipesManagerService.exception;

public class InvalidRecipesStatusException extends RuntimeException {

    public InvalidRecipesStatusException(String s) {
        super(s);
    }
}
