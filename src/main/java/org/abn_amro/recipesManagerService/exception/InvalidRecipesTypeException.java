package org.abn_amro.recipesManagerService.exception;

public class InvalidRecipesTypeException extends RuntimeException {

    public InvalidRecipesTypeException(String s) {
        super(s);
    }
}
