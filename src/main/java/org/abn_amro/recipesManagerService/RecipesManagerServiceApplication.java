package org.abn_amro.recipesManagerService;

import org.abn_amro.recipesManagerService.config.ExceptionTranslator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(ExceptionTranslator.class)
public class RecipesManagerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecipesManagerServiceApplication.class, args);
    }

}
