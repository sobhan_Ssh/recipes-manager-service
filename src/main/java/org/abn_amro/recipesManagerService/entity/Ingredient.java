package org.abn_amro.recipesManagerService.entity;

import org.abn_amro.recipesManagerService.dto.IFrom;
import org.abn_amro.recipesManagerService.dto.request.AddIngredientDTO;

import javax.persistence.*;

@Entity
@Table(name = "INGREDIENT")
public class Ingredient implements IFrom<AddIngredientDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "COUNT", nullable = false)
    private Integer count;

    @Column(name = "UNIT", nullable = false)
    private String unit;

    @Column(name = "INSTRUCTION", nullable = false, length = 5000)
    private String instruction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECIPES_ID", nullable = false)
    private Recipes recipes;

    public Long getId() {
        return id;
    }

    public Ingredient setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Ingredient setName(String name) {
        this.name = name;
        return this;
    }

    public Recipes getRecipes() {
        return recipes;
    }

    public Integer getCount() {
        return count;
    }

    public Ingredient setCount(Integer count) {
        this.count = count;
        return this;
    }

    public String getUnit() {
        return unit;
    }

    public Ingredient setUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public Ingredient setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    public Ingredient setRecipes(Recipes recipes) {
        this.recipes = recipes;
        return this;
    }

    @Override
    public Ingredient from(AddIngredientDTO source) {
        return this
                .setCount(source.getCount())
                .setUnit(source.getUnit())
                .setName(source.getName())
                .setInstruction(source.getInstruction());
    }
}
