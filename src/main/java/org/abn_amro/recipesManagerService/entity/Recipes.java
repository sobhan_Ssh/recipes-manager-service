package org.abn_amro.recipesManagerService.entity;

import org.abn_amro.recipesManagerService.dto.IFrom;
import org.abn_amro.recipesManagerService.dto.request.AddRecipesDTO;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "RECIPES")
public class Recipes implements IFrom<AddRecipesDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "SERVE_COUNT", nullable = false)
    private Integer serveCount;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "TYPE", nullable = false)
    private RecipesType type;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private RecipesStatus status;

    @Column(name = "INSTRUCTION", nullable = false, length = 5000)
    private String instruction;

    @OneToMany(mappedBy = "recipes",
            cascade = CascadeType.ALL)
    private List<Ingredient> ingredients = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public Recipes setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Recipes setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getServeCount() {
        return serveCount;
    }

    public Recipes setServeCount(Integer serveCount) {
        this.serveCount = serveCount;
        return this;
    }

    public RecipesType getType() {
        return type;
    }

    public Recipes setType(RecipesType type) {
        this.type = type;
        return this;
    }

    public RecipesStatus getStatus() {
        return status;
    }

    public Recipes setStatus(RecipesStatus status) {
        this.status = status;
        return this;
    }

    public String getInstruction() {
        return instruction;
    }

    public Recipes setInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public Recipes setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        return this;
    }

    @Override
    public Recipes from(AddRecipesDTO source) {
        return this
                .setInstruction(source.getInstruction())
                .setName(source.getName())
                .setStatus(RecipesStatus.ACTIVE)
                .setServeCount(source.getServeCount())
                .setType(RecipesType.parseRecipesType(Math.toIntExact(source.getType())));
    }
}
