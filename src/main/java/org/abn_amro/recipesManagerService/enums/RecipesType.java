package org.abn_amro.recipesManagerService.enums;

import org.abn_amro.recipesManagerService.exception.InvalidRecipesTypeException;
import org.abn_amro.recipesManagerService.util.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public enum RecipesType {

    VEGETARIAN(0),
    FASTFOOD(1),
    DAIRY(2),
    SEAFOOD(3);

    private final int value;

    RecipesType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (RecipesType type : RecipesType.values()) {
            if (type.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static RecipesType parseRecipesType(int type) {
        if (type == RecipesType.VEGETARIAN.getValue())
            return RecipesType.VEGETARIAN;
        else if (type == RecipesType.FASTFOOD.getValue())
            return RecipesType.FASTFOOD;
        else if (type == RecipesType.DAIRY.getValue())
            return RecipesType.DAIRY;
        else if (type == RecipesType.SEAFOOD.getValue())
            return RecipesType.SEAFOOD;
        else
            throw new InvalidRecipesTypeException(Constant.INVALID_RECIPES_TYPE);
    }

    public static List<RecipesType> parseRecipesTypeList(List<Integer> types) {
        List<RecipesType> recipesTypeList = new ArrayList<>();
        for (int type : types) {
        if (type == RecipesType.VEGETARIAN.getValue())
            recipesTypeList.add(RecipesType.VEGETARIAN);
        else if (type == RecipesType.FASTFOOD.getValue())
            recipesTypeList.add(RecipesType.FASTFOOD);
        else if (type == RecipesType.DAIRY.getValue())
            recipesTypeList.add(RecipesType.DAIRY);
        else if (type == RecipesType.SEAFOOD.getValue())
            recipesTypeList.add(RecipesType.SEAFOOD);
        else
            throw new InvalidRecipesTypeException(Constant.INVALID_RECIPES_TYPE);
        }
        return recipesTypeList;
    }

    public static RecipesType parseRecipesType(String name) {
        if (Objects.equals(name.toUpperCase(), RecipesType.VEGETARIAN.name()))
            return RecipesType.VEGETARIAN;
        else if (Objects.equals(name.toUpperCase(), RecipesType.FASTFOOD.name()))
            return RecipesType.FASTFOOD;
        else if (Objects.equals(name.toUpperCase(), RecipesType.DAIRY.name()))
            return RecipesType.DAIRY;
        else if (Objects.equals(name.toUpperCase(), RecipesType.SEAFOOD.name()))
            return RecipesType.SEAFOOD;
        else
            throw new InvalidRecipesTypeException(Constant.INVALID_RECIPES_TYPE);
    }
}
