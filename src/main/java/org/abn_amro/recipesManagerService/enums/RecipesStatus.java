package org.abn_amro.recipesManagerService.enums;

import org.abn_amro.recipesManagerService.exception.InvalidRecipesStatusException;
import org.abn_amro.recipesManagerService.util.Constant;

public enum RecipesStatus {

    ACTIVE(0),
    DELETED(1);

    private final int value;

    RecipesStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (RecipesStatus status : RecipesStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static RecipesStatus parseRecipesStatus(int status) {
        if (status == RecipesStatus.ACTIVE.getValue())
            return RecipesStatus.ACTIVE;
        else if (status == RecipesStatus.DELETED.getValue())
            return RecipesStatus.DELETED;
        else
            throw new InvalidRecipesStatusException(Constant.INVALID_RECIPES_STATUS);
    }
}
