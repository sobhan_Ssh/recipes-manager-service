package org.abn_amro.recipesManagerService.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.abn_amro.recipesManagerService.dto.request.*;
import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;
import org.abn_amro.recipesManagerService.repository.IngredientRepository;
import org.abn_amro.recipesManagerService.repository.RecipesRepository;
import org.abn_amro.recipesManagerService.service.impl.RecipesServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RecipesControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RecipesRepository recipesRepository;
    @Autowired
    private IngredientRepository ingredientRepository;

    @BeforeEach
    @AfterEach
    void setup() {
        ingredientRepository.deleteAll();
        recipesRepository.deleteAll();
    }

    private List<AddRecipesDTO> createTemplateAddRecipesDTOList(int listSize) {
        List<AddRecipesDTO> recipes = new ArrayList<>();
        for (int i = 1; i <= listSize; i++) {
            AddRecipesDTO recipesDTO = new AddRecipesDTO()
                    .setName("recipes name" + i)
                    .setInstruction("very long text would be here")
                    .setType(RecipesType.VEGETARIAN.getValue())
                    .setServeCount(4);
            for (int j = 1; j <= listSize; j++) {
                AddIngredientDTO ingredientDTO = new AddIngredientDTO()
                        .setName("ingredient name " + j)
                        .setInstruction("very long text would be here")
                        .setCount(4)
                        .setUnit("ingredient unit");
                recipesDTO.getIngredients().add(ingredientDTO);
            }
            recipes.add(recipesDTO);
        }
        return recipes;
    }

    private List<Recipes> createTemplateRecipesEntityList(int listSize) {
        List<Recipes> recipesList = new ArrayList<>();
        for (int i = 1; i <= listSize; i++) {
            Recipes recipes = new Recipes()
                    .setName("recipes name" + i)
                    .setInstruction("very long text would be here")
                    .setType(RecipesType.parseRecipesType(-1 + i))
                    .setServeCount(1 + i)
                    .setStatus(RecipesStatus.ACTIVE);

            for (int j = 1; j <= listSize; j++) {
                Ingredient ingredient = new Ingredient()
                        .setName("ingredient name " + j)
                        .setInstruction("very long text would be here")
                        .setCount(4)
                        .setUnit("ingredient unit");
                recipes.getIngredients().add(ingredient);
            }
            recipesList.add(recipes);
        }
        return recipesList;
    }

    /**
     * Method under test: {@link RecipesController#addRecipes(AddRecipesDTO)}
     */
    @Test
    public void givenRecipesObject_whenCreateRecipes_thenReturnSavedRecipes() throws Exception {
        // given - create recipes dto object
        AddRecipesDTO recipesDTO = createTemplateAddRecipesDTOList(1).get(0);

        // when - call '/recipes/add' POST API
        ResultActions response = mockMvc.perform(post("/recipes/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipesDTO)));

        // then - verify the result content using assert statements
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(recipesDTO.getName())))
                .andExpect(jsonPath("$.serveCount", is(recipesDTO.getServeCount())))
                .andExpect(jsonPath("$.type", is(RecipesType.VEGETARIAN.name())))
                .andExpect(jsonPath("$.status", is(RecipesStatus.ACTIVE.name())))
                .andExpect(jsonPath("$.instruction", is(recipesDTO.getInstruction())))
                .andExpect(jsonPath("$.ingredients.length()", is(recipesDTO.getIngredients().size())))
                .andExpect(jsonPath("$.id", is(notNullValue())));
    }

    /**
     * Method under test: {@link RecipesController#getAllRecipes(GetRecipesListDTO)}
     */
    @Test
    public void givenListOfRecipes_whenGetAllRecipes_thenReturnRecipesList() throws Exception {
        // given - create Recipes list entity
        List<Recipes> recipes = createTemplateRecipesEntityList(2);
        for (Recipes recipes1 : recipes) {
            List<Ingredient> ingredients = recipes1.getIngredients();
            recipes1 = recipesRepository.save(recipes1.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients)
                ingredientRepository.save(ingredient.setRecipes(recipes1));
        }

        // when -  call '/recipes/list' GET API
        ResultActions response = mockMvc.perform(get("/recipes/list"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(recipes.size())));
    }

    /**
     * Method under test: {@link RecipesController#getRecipesById(Long)}
     */
    @Test
    public void givenRecipesId_whenGetRecipesById_thenReturnRecipesObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        int ingredientsCount = ingredients.size();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients)
            ingredientRepository.save(ingredient.setRecipes(recipes));


        // when -  call '/recipes/{id}' GET API
        ResultActions response = mockMvc.perform(get("/recipes/{id}", recipes.getId()));

        // then - verify the result content using assert statements
        response
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(recipes.getName())))
                .andExpect(jsonPath("$.serveCount", is(recipes.getServeCount())))
                .andExpect(jsonPath("$.type", is(RecipesType.VEGETARIAN.name())))
                .andExpect(jsonPath("$.status", is(RecipesStatus.ACTIVE.name())))
                .andExpect(jsonPath("$.instruction", is(recipes.getInstruction())))
                .andExpect(jsonPath("$.ingredients.length()", is(ingredientsCount)))
                .andExpect(jsonPath("$.id", is(notNullValue())));
    }

    /**
     * Method under test: {@link RecipesController#deleteRecipes(Long)}
     */
    @Test
    public void givenRecipesId_whenDeleteRecipesById_thenReturnDeleteRecipesObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients)
            ingredientRepository.save(ingredient.setRecipes(recipes));

        // when -  call '/recipes/{id}' DELETE API
        ResultActions response = mockMvc.perform(delete("/recipes/{id}", recipes.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.status", is(RecipesStatus.DELETED.name())));
    }

    /**
     * Method under test: {@link RecipesController#editRecipes(EditRecipesDTO)}
     */
    @Test
    public void givenRecipesObject_whenUpdateRecipes_thenReturnUpdateRecipesObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients)
            ingredientRepository.save(ingredient.setRecipes(recipes));

        // given - create edit recipes dto
        EditRecipesDTO editRecipesDTO = new EditRecipesDTO()
                .setId(recipes.getId())
                .setName("another name")
                .setInstruction("another instruction")
                .setType(RecipesType.FASTFOOD.getValue())
                .setServeCount(8);

        // when -  call '/recipes' PUT API
        ResultActions response = mockMvc.perform(put("/recipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editRecipesDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(editRecipesDTO.getName())))
                .andExpect(jsonPath("$.serveCount", is(editRecipesDTO.getServeCount())))
                .andExpect(jsonPath("$.instruction", is(editRecipesDTO.getInstruction())))
                .andExpect(jsonPath("$.type", is(RecipesType.FASTFOOD.name())));
    }

    /**
     * Method under test: {@link RecipesController#addRecipesIngredient(AddNewIngredientDTO)}
     */
    @Test
    public void givenAddNewIngredient_whenAddIngredient_thenReturnAddedIngredientObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients) {
            ingredientRepository.save(ingredient.setRecipes(recipes));
        }

        // given - add ingredient dto
        AddNewIngredientDTO addNewIngredientDTO = new AddNewIngredientDTO()
                .setName("name")
                .setInstruction("instruction")
                .setCount(5)
                .setUnit("unit")
                .setRecipesId(recipes.getId());

        // when -  call '/recipes/ingredient' POST API
        ResultActions response = mockMvc.perform(post("/recipes/ingredient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(addNewIngredientDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(addNewIngredientDTO.getName())))
                .andExpect(jsonPath("$.count", is(addNewIngredientDTO.getCount())))
                .andExpect(jsonPath("$.instruction", is(addNewIngredientDTO.getInstruction())))
                .andExpect(jsonPath("$.unit", is(addNewIngredientDTO.getUnit())))
                .andExpect(jsonPath("$.id", is(notNullValue())));
    }

    /**
     * Method under test: {@link RecipesController#deleteRecipesIngredient(DeleteIngredientDTO)}
     */
    @Test
    public void givenDeleteIngredient_whenDeleteIngredient_thenReturnDeletedIngredientObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        List<Ingredient> persistedIngredients = new ArrayList<>();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients) {
            ingredient = ingredientRepository.save(ingredient.setRecipes(recipes));
            persistedIngredients.add(ingredient);
        }

        // given - delete ingredient dto
        DeleteIngredientDTO deleteIngredientDTO = new DeleteIngredientDTO()
                .setIngredientId(persistedIngredients.get(0).getId())
                .setRecipesId(recipes.getId());

        // when -  call '/recipes/ingredient' DELETE API
        ResultActions response = mockMvc.perform(delete("/recipes/ingredient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(deleteIngredientDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print());
    }


    /**
     * Method under test: {@link RecipesController#editRecipesIngredient(EditIngredientDTO)}
     */
    @Test
    @Disabled("TODO: when I run it as a single test it will pass, but when I Run the TestClass, Error happens")
    public void givenRecipesObject_whenUpdateRecipesIngredient_thenReturnUpdateIngredientObject() throws Exception {
        // given - create recipes entity object
        Recipes recipes = createTemplateRecipesEntityList(1).get(0);
        List<Ingredient> ingredients = recipes.getIngredients();
        List<Ingredient> persistedIngredients = new ArrayList<>();
        recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
        for (Ingredient ingredient : ingredients) {
            ingredient = ingredientRepository.save(ingredient.setRecipes(recipes));
            persistedIngredients.add(ingredient);
        }

        // given - edit ingredient dto
        EditIngredientDTO editIngredientDTO = new EditIngredientDTO()
                .setName("another name")
                .setInstruction("another instruction")
                .setCount(10)
                .setUnit("unit")
                .setRecipesId(recipes.getId())
                .setIngredientId(persistedIngredients.get(0).getId());

        // when -  call '/recipes/ingredient' PUT API
        ResultActions response = mockMvc.perform(put("/recipes/ingredient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editIngredientDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(editIngredientDTO.getName())))
                .andExpect(jsonPath("$.count", is(editIngredientDTO.getCount())))
                .andExpect(jsonPath("$.instruction", is(editIngredientDTO.getInstruction())))
                .andExpect(jsonPath("$.unit", is(editIngredientDTO.getUnit())))
                .andExpect(jsonPath("$.id", is(persistedIngredients.get(0).getId().intValue())));
    }

    /**
     * Method under test: {@link RecipesController#getFilteredRecipes(FilterRecipesDTO)}
     */
    @Test
    public void givenListOfPersistedRecipes_whenFilterByAllAvailableValuesInDTO_thenReturnNoResult() throws Exception {
        // given - create recipes entity object
        List<Recipes> recipesList = createTemplateRecipesEntityList(3);
        for (Recipes recipes : recipesList) {
            List<Ingredient> ingredients = recipes.getIngredients();
            recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients) {
                ingredientRepository.save(ingredient.setRecipes(recipes));
            }
        }

        // given - create filter ingredient dto with all available values
        FilterRecipesDTO filterRecipesDTO = new FilterRecipesDTO();
        for (int i = 1; i <= 3; i++ ) {
            filterRecipesDTO.getServeCounts().add(1 + i);
            filterRecipesDTO.getTypes().add(-1 + i);
            filterRecipesDTO.getIncludeIngredients().add("ingredient name " + i);
            filterRecipesDTO.getExcludeIngredients().add("ingredient name " + i);
        }

        // when -  call '/recipes/filter' POST API
        ResultActions response = mockMvc.perform(post("/recipes/filter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(filterRecipesDTO)));

        // then - expect to get nothing because inc and exc list are same
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(0)));
    }

    /**
     * Method under test: {@link RecipesController#getFilteredRecipes(FilterRecipesDTO)}
     */
    @Test
    public void givenListOfPersistedRecipes_whenFilterByAllAvailableTypeValues_thenReturnListHavingSameRecipesListSize() throws Exception {
        // given - create recipes entity object
        List<Recipes> recipesList = createTemplateRecipesEntityList(3);
        for (Recipes recipes : recipesList) {
            List<Ingredient> ingredients = recipes.getIngredients();
            recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients) {
                ingredientRepository.save(ingredient.setRecipes(recipes));
            }
        }

        // given - create filter ingredient dto only types
        FilterRecipesDTO filterRecipesDTO = new FilterRecipesDTO();
        filterRecipesDTO.getTypes().addAll(List.of(0,1,2,3));

        // when -  call '/recipes/filter' POST API
        ResultActions response = mockMvc.perform(post("/recipes/filter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(filterRecipesDTO)));

        // then - get all available recipes
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(3)));
    }

    /**
     * Method under test: {@link RecipesController#getFilteredRecipes(FilterRecipesDTO)}
     */
    @Test
    public void givenListOfPersistedRecipes_whenFilterOneExcludedIngredient_thenReturnNothing() throws Exception {
        // given - create recipes entity object
        List<Recipes> recipesList = createTemplateRecipesEntityList(3);
        for (Recipes recipes : recipesList) {
            List<Ingredient> ingredients = recipes.getIngredients();
            recipes = recipesRepository.save(recipes.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients) {
                ingredientRepository.save(ingredient.setRecipes(recipes));
            }
        }

        // given - create filter ingredient dto only types
        FilterRecipesDTO filterRecipesDTO = new FilterRecipesDTO();
        filterRecipesDTO.getExcludeIngredients().add("ingredient name 1");

        // when -  call '/recipes/filter' POST API
        ResultActions response = mockMvc.perform(post("/recipes/filter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(filterRecipesDTO)));

        // then - expect to get nothing because all recipes contain specific ingredient
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(0)));
    }
}
