package org.abn_amro.recipesManagerService.repository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.abn_amro.recipesManagerService.RecipesManagerServiceApplication;
import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest(classes = RecipesManagerServiceApplication.class)
class RecipesRepositoryUnitTest {

    @Autowired
    private RecipesRepository recipesRepository;
    @Autowired
    private IngredientRepository ingredientRepository;
    @AfterEach
    @BeforeEach
    private void setup() {
        ingredientRepository.deleteAll();
        recipesRepository.deleteAll();;
    }

    private List<Recipes> createTemplateRecipesEntityList(int listSize) {
        List<Recipes> recipesList = new ArrayList<>();
        for (int i = 1; i <= listSize; i++) {
            Recipes recipes = new Recipes()
                    .setName("recipes name" + i)
                    .setInstruction("very long text would be here")
                    .setType(RecipesType.parseRecipesType(-1 + i))
                    .setServeCount(1 + i)
                    .setStatus(RecipesStatus.ACTIVE);

            for (int j = 1; j <= listSize; j++) {
                Ingredient ingredient = new Ingredient()
                        .setName("ingredient name " + j)
                        .setInstruction("very long text would be here")
                        .setCount(4)
                        .setUnit("ingredient unit");
                recipes.getIngredients().add(ingredient);
            }
            recipesList.add(recipes);
        }
        return recipesList;
    }

    /**
     * Method under test: {@link RecipesRepository#findByTypeAndStatus(RecipesType, RecipesStatus, Pageable)}
     */
    @Test
    void testFindByTypeAndStatus() {
        List<Recipes> recipes = createTemplateRecipesEntityList(2);
        for (Recipes recipes1 : recipes) {
            List<Ingredient> ingredients = recipes1.getIngredients();
            recipes1 = recipesRepository.save(recipes1.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients)
                ingredientRepository.save(ingredient.setRecipes(recipes1));
        }

        var temp = recipesRepository.findByTypeAndStatus(RecipesType.VEGETARIAN, RecipesStatus.ACTIVE, PageRequest.of(0,10));
        var temp1 = recipesRepository.findByTypeAndStatus(RecipesType.FASTFOOD, RecipesStatus.ACTIVE, PageRequest.of(0,10));
        var temp2 = recipesRepository.findByTypeAndStatus(RecipesType.DAIRY, RecipesStatus.ACTIVE, PageRequest.of(0,10));
        var temp3 = recipesRepository.findByTypeAndStatus(RecipesType.SEAFOOD, RecipesStatus.ACTIVE, PageRequest.of(0,10));
        assertEquals(temp.getContent().size(), 1);
        assertEquals(temp1.getContent().size(), 1);
        assertEquals(temp2.getContent().size(), 0);
        assertEquals(temp3.getContent().size(), 0);
    }

    /**
     * Method under test: {@link RecipesRepository#findByStatus(RecipesStatus, Pageable)}
     */
    @Test
    void testFindByStatus() {
        List<Recipes> recipes = createTemplateRecipesEntityList(2);
        for (Recipes recipes1 : recipes) {
            List<Ingredient> ingredients = recipes1.getIngredients();
            recipes1 = recipesRepository.save(recipes1.setIngredients(new ArrayList<>()));
            for (Ingredient ingredient : ingredients)
                ingredientRepository.save(ingredient.setRecipes(recipes1));
        }
        var temp = recipesRepository.findAll();
        assertEquals(temp.size(), 2);

        recipesRepository.save(temp.get(0).setStatus(RecipesStatus.DELETED));

        var tempActive = recipesRepository.findByStatus(RecipesStatus.ACTIVE,  PageRequest.of(0,10));
        assertEquals(tempActive.getContent().size(), 1);

        var tempDelete = recipesRepository.findByStatus(RecipesStatus.DELETED,  PageRequest.of(0,10));
        assertEquals(tempDelete.getContent().size(), 1);

    }

    /**
     * Method under test: {@link RecipesRepository#findFilteredRecipes(List, List, List, List, RecipesStatus, Pageable)}
     */
    @Test
    @Disabled
    void testFindFilteredRecipes() {
        ArrayList<RecipesType> recipesTypeList = new ArrayList<>();
        ArrayList<Integer> integerList = new ArrayList<>();
        ArrayList<String> stringList = new ArrayList<>();
        recipesRepository.findFilteredRecipes(recipesTypeList, integerList, stringList, new ArrayList<>(),
                RecipesStatus.ACTIVE, null);
    }
}

