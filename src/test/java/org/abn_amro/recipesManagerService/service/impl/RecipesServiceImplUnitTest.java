package org.abn_amro.recipesManagerService.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.abn_amro.recipesManagerService.dto.request.*;

import org.abn_amro.recipesManagerService.dto.response.IngredientDTO;
import org.abn_amro.recipesManagerService.dto.response.ListResponseDTO;
import org.abn_amro.recipesManagerService.dto.response.RecipesDTO;
import org.abn_amro.recipesManagerService.entity.Ingredient;
import org.abn_amro.recipesManagerService.entity.Recipes;
import org.abn_amro.recipesManagerService.enums.RecipesStatus;
import org.abn_amro.recipesManagerService.enums.RecipesType;
import org.abn_amro.recipesManagerService.exception.AlreadyDeletedException;
import org.abn_amro.recipesManagerService.exception.NotFoundException;
import org.abn_amro.recipesManagerService.repository.IngredientRepository;
import org.abn_amro.recipesManagerService.repository.RecipesRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RecipesServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RecipesServiceImplUnitTest {
    @MockBean
    private IngredientRepository ingredientRepository;

    @MockBean
    private RecipesRepository recipesRepository;

    @Autowired
    private RecipesServiceImpl recipesServiceImpl;

    /**
     * Method under test: {@link RecipesServiceImpl#getRecipesById(Long)}
     */
    @Test
    void testGetRecipesById() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);
        RecipesDTO actualRecipesById = recipesServiceImpl.getRecipesById(123L);
        assertEquals(123L, actualRecipesById.getId().longValue());
        assertEquals(RecipesType.VEGETARIAN, actualRecipesById.getType());
        assertEquals(RecipesStatus.ACTIVE, actualRecipesById.getStatus());
        assertEquals(3, actualRecipesById.getServeCount().intValue());
        assertEquals("Name", actualRecipesById.getName());
        assertEquals("Instruction", actualRecipesById.getInstruction());
        assertTrue(actualRecipesById.getIngredients().isEmpty());
        verify(recipesRepository).findById((Long) any());
    }




    /**
     * Method under test: {@link RecipesServiceImpl#getRecipesList(GetRecipesListDTO)}
     */
    @Test
    void testGetRecipesList() {
        ArrayList<Recipes> recipesList = new ArrayList<>();
        when(recipesRepository.findByTypeAndStatus((RecipesType) any(), (RecipesStatus) any(), (Pageable) any()))
                .thenReturn(new PageImpl<>(recipesList));

        GetRecipesListDTO getRecipesListDTO = new GetRecipesListDTO();
        getRecipesListDTO.setPage(1);
        getRecipesListDTO.setSize(3);
        getRecipesListDTO.setType(1);
        assertEquals(recipesList, recipesServiceImpl.getRecipesList(getRecipesListDTO).getData());
        verify(recipesRepository).findByTypeAndStatus((RecipesType) any(), (RecipesStatus) any(), (Pageable) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#getRecipesList(GetRecipesListDTO)}
     */
    @Test
    void testGetRecipesList2() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);

        ArrayList<Recipes> recipesList = new ArrayList<>();
        recipesList.add(recipes);
        PageImpl<Recipes> pageImpl = new PageImpl<>(recipesList);
        when(recipesRepository.findByTypeAndStatus((RecipesType) any(), (RecipesStatus) any(), (Pageable) any()))
                .thenReturn(pageImpl);

        GetRecipesListDTO getRecipesListDTO = new GetRecipesListDTO();
        getRecipesListDTO.setPage(1);
        getRecipesListDTO.setSize(3);
        getRecipesListDTO.setType(1);
        ListResponseDTO<RecipesDTO> actualRecipesList = recipesServiceImpl.getRecipesList(getRecipesListDTO);
        assertEquals(1, actualRecipesList.getData().size());
        assertEquals(1, actualRecipesList.getSize());
        assertEquals(1, actualRecipesList.getPage());
        verify(recipesRepository).findByTypeAndStatus((RecipesType) any(), (RecipesStatus) any(), (Pageable) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#addRecipes(AddRecipesDTO)}
     */
    @Test
    void testAddRecipes() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);

        when(recipesRepository.save((Recipes) any())).thenReturn(recipes);

        AddRecipesDTO addRecipesDTO = new AddRecipesDTO();
        addRecipesDTO.setIngredients(new ArrayList<>());
        addRecipesDTO.setInstruction("Instruction");
        addRecipesDTO.setName("Name");
        addRecipesDTO.setServeCount(3);
        addRecipesDTO.setType(1);
        RecipesDTO actualAddRecipesResult = recipesServiceImpl.addRecipes(addRecipesDTO);
        assertEquals(123L, actualAddRecipesResult.getId().longValue());
        assertEquals(RecipesType.VEGETARIAN, actualAddRecipesResult.getType());
        assertEquals(RecipesStatus.ACTIVE, actualAddRecipesResult.getStatus());
        assertEquals(3, actualAddRecipesResult.getServeCount().intValue());
        assertEquals("Name", actualAddRecipesResult.getName());
        assertEquals("Instruction", actualAddRecipesResult.getInstruction());
        assertTrue(actualAddRecipesResult.getIngredients().isEmpty());
        verify(recipesRepository).save((Recipes) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#addRecipes(AddRecipesDTO)}
     */
    @Test
    void testAddRecipes2() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);

        Ingredient ingredient = new Ingredient();
        ingredient.setCount(3);
        ingredient.setId(123L);
        ingredient.setInstruction("Instruction");
        ingredient.setName("Name");
        ingredient.setRecipes(recipes);
        ingredient.setUnit("Unit");

        ArrayList<Ingredient> ingredientList = new ArrayList<>();
        ingredientList.add(ingredient);
        recipes.setIngredients(ingredientList);

        when(recipesRepository.save((Recipes) any())).thenReturn(recipes);

        AddRecipesDTO addRecipesDTO = new AddRecipesDTO();
        addRecipesDTO.setIngredients(new ArrayList<>());
        addRecipesDTO.setInstruction("Instruction");
        addRecipesDTO.setName("Name");
        addRecipesDTO.setServeCount(3);
        addRecipesDTO.setType(1);

        AddIngredientDTO addIngredientDTO = new AddIngredientDTO();
        addIngredientDTO.setCount(3);
        addIngredientDTO.setInstruction("Instruction");
        addIngredientDTO.setName("Name");
        addIngredientDTO.setUnit("Unit");

        ArrayList<AddIngredientDTO> ingredientDTOList = new ArrayList<>();
        ingredientDTOList.add(addIngredientDTO);
        addRecipesDTO.setIngredients(ingredientDTOList);
        RecipesDTO actualAddRecipesResult = recipesServiceImpl.addRecipes(addRecipesDTO);
        assertEquals(123L, actualAddRecipesResult.getId().longValue());
        assertEquals(RecipesType.VEGETARIAN, actualAddRecipesResult.getType());
        assertEquals(RecipesStatus.ACTIVE, actualAddRecipesResult.getStatus());
        assertEquals(3, actualAddRecipesResult.getServeCount().intValue());
        assertEquals("Name", actualAddRecipesResult.getName());
        assertEquals("Instruction", actualAddRecipesResult.getInstruction());
        verify(recipesRepository).save((Recipes) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#deleteRecipes(Long)}
     */
    @Test
    void testDeleteRecipes() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);

        Recipes recipes1 = new Recipes();
        recipes1.setId(123L);
        recipes1.setIngredients(new ArrayList<>());
        recipes1.setInstruction("Instruction");
        recipes1.setName("Name");
        recipes1.setServeCount(3);
        recipes1.setStatus(RecipesStatus.ACTIVE);
        recipes1.setType(RecipesType.VEGETARIAN);
        when(recipesRepository.save((Recipes) any())).thenReturn(recipes1);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);
        RecipesDTO actualDeleteRecipesResult = recipesServiceImpl.deleteRecipes(123L);
        assertEquals(123L, actualDeleteRecipesResult.getId().longValue());
        assertEquals(RecipesType.VEGETARIAN, actualDeleteRecipesResult.getType());
        assertEquals(RecipesStatus.ACTIVE, actualDeleteRecipesResult.getStatus());
        assertEquals(3, actualDeleteRecipesResult.getServeCount().intValue());
        assertEquals("Name", actualDeleteRecipesResult.getName());
        assertEquals("Instruction", actualDeleteRecipesResult.getInstruction());
        assertTrue(actualDeleteRecipesResult.getIngredients().isEmpty());
        verify(recipesRepository).save((Recipes) any());
        verify(recipesRepository).findById((Long) any());
    }


    /**
     * Method under test: {@link RecipesServiceImpl#editRecipes(EditRecipesDTO)}
     */
    @Test
    void testEditRecipes() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);

        Recipes recipes1 = new Recipes();
        recipes1.setId(123L);
        recipes1.setIngredients(new ArrayList<>());
        recipes1.setInstruction("Instruction");
        recipes1.setName("Name");
        recipes1.setServeCount(3);
        recipes1.setStatus(RecipesStatus.ACTIVE);
        recipes1.setType(RecipesType.VEGETARIAN);
        when(recipesRepository.save((Recipes) any())).thenReturn(recipes1);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);

        EditRecipesDTO editRecipesDTO = new EditRecipesDTO();
        editRecipesDTO.setId(123L);
        editRecipesDTO.setInstruction("Instruction");
        editRecipesDTO.setName("Name");
        editRecipesDTO.setServeCount(3);
        editRecipesDTO.setType(1);
        RecipesDTO actualEditRecipesResult = recipesServiceImpl.editRecipes(editRecipesDTO);
        assertEquals(123L, actualEditRecipesResult.getId().longValue());
        assertEquals(RecipesType.VEGETARIAN, actualEditRecipesResult.getType());
        assertEquals(RecipesStatus.ACTIVE, actualEditRecipesResult.getStatus());
        assertEquals(3, actualEditRecipesResult.getServeCount().intValue());
        assertEquals("Name", actualEditRecipesResult.getName());
        assertEquals("Instruction", actualEditRecipesResult.getInstruction());
        assertTrue(actualEditRecipesResult.getIngredients().isEmpty());
        verify(recipesRepository).save((Recipes) any());
        verify(recipesRepository).findById((Long) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#addRecipesIngredient(AddNewIngredientDTO)}
     */
    @Test
    void testAddRecipesIngredient() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);

        Recipes recipes1 = new Recipes();
        recipes1.setId(123L);
        recipes1.setIngredients(new ArrayList<>());
        recipes1.setInstruction("Instruction");
        recipes1.setName("Name");
        recipes1.setServeCount(3);
        recipes1.setStatus(RecipesStatus.ACTIVE);
        recipes1.setType(RecipesType.VEGETARIAN);

        Ingredient ingredient = new Ingredient();
        ingredient.setCount(3);
        ingredient.setId(123L);
        ingredient.setInstruction("Instruction");
        ingredient.setName("Name");
        ingredient.setRecipes(recipes1);
        ingredient.setUnit("Unit");
        when(ingredientRepository.save((Ingredient) any())).thenReturn(ingredient);

        AddNewIngredientDTO addNewIngredientDTO = new AddNewIngredientDTO();
        addNewIngredientDTO.setCount(3);
        addNewIngredientDTO.setInstruction("Instruction");
        addNewIngredientDTO.setName("Name");
        addNewIngredientDTO.setRecipesId(123L);
        addNewIngredientDTO.setUnit("Unit");
        IngredientDTO actualAddRecipesIngredientResult = recipesServiceImpl.addRecipesIngredient(addNewIngredientDTO);
        assertEquals(3, actualAddRecipesIngredientResult.getCount().intValue());
        assertEquals("Unit", actualAddRecipesIngredientResult.getUnit());
        assertEquals("Name", actualAddRecipesIngredientResult.getName());
        assertEquals("Instruction", actualAddRecipesIngredientResult.getInstruction());
        assertEquals(123L, actualAddRecipesIngredientResult.getId().longValue());
        verify(recipesRepository).findById((Long) any());
        verify(ingredientRepository).save((Ingredient) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#deleteRecipesIngredient(DeleteIngredientDTO)}
     */
    @Test
    void testDeleteRecipesIngredient() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);

        DeleteIngredientDTO deleteIngredientDTO = new DeleteIngredientDTO();
        deleteIngredientDTO.setIngredientId(123L);
        deleteIngredientDTO.setRecipesId(123L);
        assertThrows(NotFoundException.class, () -> recipesServiceImpl.deleteRecipesIngredient(deleteIngredientDTO));
        verify(recipesRepository).findById((Long) any());
    }

    /**
     * Method under test: {@link RecipesServiceImpl#editRecipesIngredient(EditIngredientDTO)}
     */
    @Test
    void testEditRecipesIngredient() {
        Recipes recipes = new Recipes();
        recipes.setId(123L);
        recipes.setIngredients(new ArrayList<>());
        recipes.setInstruction("Instruction");
        recipes.setName("Name");
        recipes.setServeCount(3);
        recipes.setStatus(RecipesStatus.ACTIVE);
        recipes.setType(RecipesType.VEGETARIAN);
        Optional<Recipes> ofResult = Optional.of(recipes);
        when(recipesRepository.findById((Long) any())).thenReturn(ofResult);

        EditIngredientDTO editIngredientDTO = new EditIngredientDTO();
        editIngredientDTO.setCount(3);
        editIngredientDTO.setIngredientId(123L);
        editIngredientDTO.setInstruction("Instruction");
        editIngredientDTO.setName("Name");
        editIngredientDTO.setRecipesId(123L);
        editIngredientDTO.setUnit("Unit");
        assertThrows(NotFoundException.class, () -> recipesServiceImpl.editRecipesIngredient(editIngredientDTO));
        verify(recipesRepository).findById((Long) any());
    }
}

