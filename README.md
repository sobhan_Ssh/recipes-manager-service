# Recipes Management Service

The goal was to develop a very simple APIs in order to support **CRUD** actions for **Recipes** entity which contains list of **Ingredients**, and the support of having **Filtering** ability over **Recipes** and it's **Ingredients** that are persisted in Database.

This is a sample project implemented using below stack:
<ul>
<li>JAVA, as project base language</li>
<li>SPRING-BOOT, as main framework</li>
<li>H2-DATABASE, an in-memory database which needs no configuration</li>
<li>GRADLE, as our build tools and dependency manager</li>
<li>JUNIT & SPRING-TEST & Mockito are also used in order to write integration tests for APIs and unit tests for service and repository layer</li>
</ul>

Based on the project requirements and business logic I have implemented several APIs
which includes: 
<ul>
<li>API services in order to create, update, delete and Get Recipes</li>
<li>API services in order to add, update and delete Ingredients for specific Recipes</li>
</ul>
Also, I have implemented a service to support Filtering over Recipes & Ingredients with four different parameters:
<ul>
<li>Recipes Type, which can be VEGETARIAN, FASTFOOD, DAIRY, SEAFOOD</li>
<li>Recipes serving number</li>
<li>Recipes including specific Ingredients</li>
<li>Recipes excluding specific Ingredients</li>
</ul>

- There is also an export of API services for using in Postman application, this file exists in /resources folder with name of **RecipesManagerService.postman_collection.json**

- Also I have enabled **H2 Database Console**, you can use in your browse with following URL: [H2-Console](localhost:2121/h2-console), when project is running, needed parameters are :
1.  User Name: root
1.  Password: 123456
1.  Url: jdbc:h2:mem:RMS-DB






